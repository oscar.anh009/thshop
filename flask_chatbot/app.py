# chatbot.py
from flask import Flask, request, jsonify
from flask_cors import CORS
from configparser import ConfigParser
from chatbot import ChatBot  # Import ChatBot class from your chatbot.py module

app = Flask(__name__)
CORS(app)

config = ConfigParser()
config.read('credentials.ini')
api_key = config['thshop_ai']['API_KEY']

chatbot = ChatBot(api_key=api_key)
chatbot.start_conversation()

@app.route('/api/chatbot', methods=['POST'])
def chatbot_route():
    try:
        data = request.get_json()
        user_input = data['user_input']
        response = chatbot.send_prompt(user_input)
        return jsonify({'response': response})
    except Exception as e:
        return jsonify({'error': str(e)}), 500

if __name__ == '__main__':
    app.run(port=5000)
